import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalHeader } from "../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { downloadUrl, ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { createElement, insertStyleTag, waitForElement } from "../typescript-lib/src/shared/dom"
import { settings } from "./constants/constants"
import { beatmap, beatmapInfo, discussionInfo } from "./constants/types"
import { useFeature } from "./settings"

/**
 * This functions intitialises all the beatmap related modifications.
 */
export async function insertBeatmapModifications() {
    do {
        await wait(500)
        if (!location.pathname.includes("discussion")) {
            await waitForElement(".beatmapset-header__buttons")
            useFeature(settings.beatmaps.showDotOsuData.storageKey, insertGetDotOsuButton)
            useFeature(settings.beatmaps.showExpandDescription.storageKey, insertBeatmapDescriptionExpander)

            const apiInfo = JSON.parse((await waitForElement("#json-beatmapset")).innerHTML) as beatmap
            useFeature(settings.beatmaps.showOMDBBUtton.storageKey, () => {
                insertBeatmapInfoHeaderButton("database", "Rate on OMDB", "open-omdb-button", () => {
                    window.open(`https://omdb.nyahh.net/mapset/${apiInfo.id!}`)
                })
            })
            useFeature(settings.beatmaps.showBeatmapCoverButton.storageKey, () => {
                insertBeatmapInfoHeaderButton("image", "Open background", "open-cover-button", () => {
                    window.open(`https://assets.ppy.sh/beatmaps/${apiInfo.id!}/covers/raw.jpg`)
                })
            })
        } else {
            await waitForElement(".beatmap-discussions-header-bottom__content")
            useFeature(settings.beatmaps.showDownloadOnDiscussion.storageKey, insertDownloadButtonsOnDiscussion)
        }
        useFeature(settings.beatmaps.showDetailedInfo.storageKey, insertDetailedInfoButton)
        await wait(2000)
    } while (location.pathname.split("/")[1] === "beatmapsets")
}

/**
 * adds beatmap dl buttons to the discussion page
 */
function insertDownloadButtonsOnDiscussion() {
    if (document.querySelector(".discussion-download") != null) {
        return
    }

    const target = ensuredSelector(".beatmap-discussions-header-bottom__content")

    const discussionInfo = JSON.parse(ensuredSelector<HTMLScriptElement>("#json-beatmapset-discussion").innerText) as discussionInfo

    const container = createElement("div", {
        className: "beatmap-discussions-header-bottom__details discussion-download",
        style: {
            display: "flex",
            gap: "10px",
            minHeight: "32.5px",
        },
    })

    container.append(createDlButton(`https://osu.ppy.sh/beatmapsets/${discussionInfo.beatmapset.id}/download`, "Download"))
    if (discussionInfo.beatmapset.video) {
        container.append(createDlButton(`https://osu.ppy.sh/beatmapsets/${discussionInfo.beatmapset.id}/download?noVideo=1`, "No Video"))
    }
    container.append(createDlButton(`osu://b/${discussionInfo.beatmapset.beatmaps[0].id}`, "osu!direct"))

    target.append(container)

    function createDlButton(url: string, label: string) {
        const el = createElement("a", {
            className: "btn-osu-big btn-osu-big--full",
            children: [
                createElement("span", {
                    attributes: {
                        innerText: label,
                    },
                    style: {
                        maxWidth: "max-content",
                        paddingLeft: "4px",
                    },
                }),
                createElement("span", { className: "fas fa-download", style: { paddingRight: "10px" } }),
            ],
            attributes: {
                href: url,
            },
            style: {
                padding: "6px",
                display: "flex",
                gap: "4px",
                alignItems: "center",
                justifyContent: "space-between",
            },
        })

        // dont ask me what turbolinks is, but it makes the download work
        el.dataset.turbolinks = "false"

        return el
    }
}

/**
 * This function gets the .osu data for the currently selected difficulty
 */
function insertGetDotOsuButton() {
    if (document.querySelector(".get-osu-data") != null) {
        return
    }

    insertBeatmapInfoHeaderButton("file-code", "Download .osu data", "get-osu-data", () => {
        downloadUrl(`https://osu.ppy.sh/osu/${window.location.toString().split("/")[window.location.toString().split("/").length - 1]}`, Date.now() + ".osu")
    })
}

/**
 * inserts a modal that displays detailed metadata info for the beatmap
 */
function insertDetailedInfoButton() {
    if (document.querySelector(".detailed-info") != null) {
        return
    }

    if (window.location.pathname.includes("discussion")) {
        // dicussion page
        const info = JSON.parse(ensuredSelector<HTMLElement>("#json-beatmapset-discussion").innerText).beatmapset as beatmapInfo

        ensuredSelector(".beatmap-discussions-header-bottom__content").append(
            createElement("div", {
                className: "beatmap-discussions-header-bottom__details discussion-download",
                style: {
                    display: "flex",
                    gap: "10px",
                    minHeight: "32.5px",
                },
                children: [
                    createElement("button", {
                        className: "btn-osu-big btn-osu-big--full detailed-info",
                        children: [
                            createElement("span", {
                                attributes: {
                                    innerText: "Detailed Beatmap Info",
                                },
                                style: {
                                    paddingLeft: "4px",
                                },
                            }),
                            createElement("span", { className: "fas fa-info-circle", style: { paddingRight: "10px" } }),
                        ],
                        style: {
                            padding: "6px",
                            display: "flex",
                            gap: "4px",
                            alignItems: "center",
                            justifyContent: "space-between",
                        },
                        events: [
                            {
                                type: "click",
                                handler: () => {
                                    openBeatmapDetailedInfoModal(info)
                                },
                            },
                        ],
                    }),
                ],
            })
        )
    } else {
        // beatmap info page
        const info = JSON.parse(ensuredSelector<HTMLScriptElement>("#json-beatmapset").innerText) as beatmapInfo

        insertBeatmapDescriptionHoverButton(
            "eye",
            "View detailed information",
            "detailed-info",
            () => {
                openBeatmapDetailedInfoModal(info)
            },
            ensuredSelector<HTMLElement>(".beatmapset-info__box:nth-child(2)")
        )
    }
}

function openBeatmapDetailedInfoModal(info: beatmapInfo) {
    const modal = new OsuModal(new OsuModalHeader("Detailed Metadata Info"))
    const modalWrapper = new ModalWrapper(modal)
    modal.style.maxWidth = "40vw"

    modal.addContent(createInfoRow("Artist", info.artist))
    modal.addContent(createInfoRow("Artist (Unicode)", info.artist_unicode))
    modal.addContent(createInfoRow("Source", info.source))
    modal.addContent(createInfoRow("Title", info.title))
    modal.addContent(createInfoRow("Title (Unicode)", info.title_unicode))
    modal.addContent(
        createElement("div", {
            style: {
                display: "flex",
                flexDirection: "column",
                gap: "4px",
            },
            children: [
                createElement("span", {
                    style: {
                        fontWeight: "bold",
                    },
                    attributes: {
                        innerText: `Tags:`,
                    },
                }),

                createElement("div", {
                    style: {
                        display: "flex",
                        gap: "4px",
                        flexWrap: "wrap",
                    },
                    children: [
                        ...info.tags.split(" ").map((tag) => {
                            return createElement("span", {
                                style: {
                                    padding: "2px 4px",
                                    backgroundColor: "#22282a",
                                    borderRadius: "2px",
                                },
                                attributes: {
                                    innerText: tag,
                                },
                            })
                        }),
                    ],
                }),
            ],
        })
    )

    document.body.append(modalWrapper)

    function createInfoRow(label: string, content: string) {
        return createElement("div", {
            style: {
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                gap: "4px",
                borderBottom: "solid 1px rgba(255, 255, 255, 0.1)",
            },
            children: [
                createElement("span", {
                    style: {
                        fontWeight: "bold",
                    },
                    attributes: {
                        innerText: `${label}:`,
                    },
                }),
                createElement("span", {
                    attributes: {
                        innerText: content,
                    },
                }),
            ],
        })
    }
}

function insertBeatmapDescriptionExpander() {
    if (document.querySelector(".description-expander") != null) {
        return
    }
    insertBeatmapDescriptionHoverButton(
        "eye",
        "View full description",
        "description-expander",
        () => {
            const modal = new OsuModal(new OsuModalHeader("Full Description"))
            const modalWrapper = new ModalWrapper(modal)
            modal.style.backgroundColor = "hsl(var(--hsl-b5))"
            modal.addContent(getBeatmapDescription())
            document.body.append(modalWrapper)
        },
        ensuredSelector<HTMLElement>(".beatmapset-info__box")
    )
}

function getBeatmapDescription() {
    const desc = ensuredSelector<HTMLElement>(".beatmapset-info__box .bbcode")

    desc.querySelectorAll<HTMLElement>(".bbcode-spoilerbox:not(.js-spoilerbox--open)").forEach((e) => {
        const closedButton = e.querySelector<HTMLElement>(".bbcode-spoilerbox__link")
        if (closedButton) {
            closedButton.click()
        }
    })

    insertStyleTag(`
        .enhanced-modal .bbcode{
            max-width: 60vw;
            max-height: 80vh;
            overflow-y: auto;
            padding: 0px 8px;
        }    
    `)

    return desc.cloneNode(true) as HTMLElement
}

function insertBeatmapInfoHeaderButton(icon: string, tooltip: string, className: string, eventHandler: Function) {
    if (document.querySelector(`.${className}`) != null) {
        return
    }

    const moreButton = document.querySelector(".beatmapset-header__more")
    const container = ensuredSelector(".beatmapset-header__buttons")

    const button = createElement("button", {
        className: `${className} btn-osu-big btn-osu-big--beatmapset-header-square`,
        children: [
            createElement("span", {
                className: "btn-osu-big__content btn-osu-big__content--center",
                children: [
                    createElement("span", {
                        className: "btn-osu-big__icon",
                        children: [
                            createElement("span", {
                                className: "fa fa-fw",
                                children: [
                                    createElement("span", {
                                        className: `fa fa-${icon}`,
                                    }),
                                ],
                            }),
                        ],
                    }),
                ],
            }),
        ],
        attributes: {
            onclick: () => eventHandler(),
            title: tooltip,
        },
    })

    moreButton != null ? moreButton.before(button) : container.append(button)
}

function insertBeatmapDescriptionHoverButton(icon: string, tooltip: string, className: string, eventHandler: Function, target: HTMLElement) {
    insertStyleTag(`
    .beatmapset-info__edit-button ~ .beatmapset-info__edit-button {
        right: 34px;
    }
    .beatmapset-info__edit-button ~ .beatmapset-info__edit-button ~ .beatmapset-info__edit-button {
        right: calc(2* 34px);
    }
    .beatmapset-info__edit-button ~ .beatmapset-info__edit-button ~ .beatmapset-info__edit-button ~ .beatmapset-info__edit-button {
        right: calc(3* 34px);
    }
    .beatmapset-info__edit-button ~ .beatmapset-info__edit-button ~ .beatmapset-info__edit-button ~ .beatmapset-info__edit-button ~ .beatmapset-info__edit-button {
        right: calc(4* 34px);
    }
    `)

    target.append(
        createElement("div", {
            className: "beatmapset-info__edit-button",
            children: [
                createElement("button", {
                    className: `btn-circle ${className}`,
                    children: [
                        createElement("span", {
                            className: "btn-circle__content",
                            children: [
                                createElement("span", {
                                    className: `fas fa-${icon}`,
                                }),
                            ],
                        }),
                    ],
                    attributes: {
                        title: tooltip,
                        onclick: () => eventHandler(),
                    },
                }),
            ],
        })
    )
}
