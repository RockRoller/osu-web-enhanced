import { OsuModal } from "../../../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../../../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../../../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { createElement, insertStyleTag } from "../../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../../typescript-lib/src/shared/themes"
import { MessageEditorListItem } from "../../components/message-editor-list-item"
import { customMessageTypes } from "../../constants/types"
import { generateCustomMessageStorageKey, getSortedCustomMessages } from "../storage"
import { createMessageEditor } from "./MessageEditor"
import { createMessageImport } from "./MessageImport"
import { createMessageSelectModal } from "./MessageSelector"

/**
 * This function creates the managment view for custom messages
 * @param modalWrapper modalwrapper to be inserted into
 * @param customMessageType
 * @returns modal
 */
export function createMessagesManager(customMessageType: customMessageTypes) {
    const modalHeader = new OsuModalHeader("Manage Messages")
    const modal = new OsuModal(modalHeader)

    insertStyleTag(`
    .toggle-active .toggle { right: 3px !important }
    .toggle-active .toggle-bg { border-color: ${osuTheme.successColor} !important }
    `)

    modal.addContent(
        createElement("div", {
            style: {
                display: "grid",
                gap: "8px",
            },
            children: [
                ...getSortedCustomMessages(customMessageType).map((customMessage) => {
                    return new MessageEditorListItem(customMessage, modal, customMessageType)
                }),
            ],
        })
    )

    modal.addModalButton(
        new OsuModalButton(
            "Back",
            () => {
                modal.replaceWith(createMessageSelectModal(customMessageType))
            },
            "secondary",
            "back"
        )
    )

    modal.addModalButton(
        new OsuModalButton(
            "New Message",
            () => {
                modal.replaceWith(
                    createMessageEditor(
                        {
                            messageOptions: {
                                title: "",
                                message: "",
                                parameters: [],
                            },
                            storageKey: generateCustomMessageStorageKey(customMessageType),
                        },
                        "new",
                        customMessageType
                    )
                )
            },
            "primary",
            "new"
        )
    )

    modalHeader.addIconButton("download", "Import Message", () => {
        modal.replaceWith(createMessageImport(customMessageType))
    })
    return modal
}
