import { OsuModal } from "../../../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../../../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../../../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { sendChatMessage } from "../../chat"
import { MessageSelectOption } from "../../components/chat-message-option"
import { OsuInputDropdown } from "../../components/input-dropdown"
import { customMessageTypes } from "../../constants/types"
import { insertForumMessage } from "../../forum/index"
import { getSortedCustomMessages } from "../storage"
import { createMessagesManager } from "./MessageManager"

/**
 * This function creates the message selector modal
 * @param modalWrapper
 * @param customMessageType
 * @return modal
 */
export function createMessageSelectModal(customMessageType: customMessageTypes) {
    const modalHeader = new OsuModalHeader("Select Message")
    const modal = new OsuModal(modalHeader)

    modalHeader.addIconButton("edit", "Manage Messages", () => {
        modal.replaceWith(createMessagesManager(customMessageType))
    })

    const selector = new OsuInputDropdown()
    getSortedCustomMessages(customMessageType).forEach((customMessage) => {
        selector.appendOption(new MessageSelectOption(customMessage))
    })

    modal.addContent(selector)

    switch (customMessageType) {
        case "chat":
            modal.addModalButton(
                new OsuModalButton(
                    "Send",
                    () => {
                        sendChatMessage(getSelectedOption())
                    },
                    "primary",
                    "send"
                )
            )
            break
        case "forum":
            modal.addModalButton(
                new OsuModalButton(
                    "Insert",
                    () => {
                        insertForumMessage(getSelectedOption())
                    },
                    "primary",
                    "insert-template"
                )
            )
            break
    }

    return modal

    function getSelectedOption() {
        return (selector.getSelector().options[selector.getSelector().selectedIndex] as MessageSelectOption).getCustomMessage()
    }
}
