export function hideNotificationsCount() {
    let previousTitle = ""
    const observer = new MutationObserver(() => {
        if (document.title !== previousTitle) {
            let newTitle = document.title

            if (document.title.match(/^\(\d*\) .*/)) {
                newTitle = newTitle.replace(/^\(\d*\) /, "")
            }

            document.title = newTitle
            previousTitle = newTitle
        }
    })
    observer.observe(document, { subtree: true, childList: false, attributes: true, characterData: false })
}
