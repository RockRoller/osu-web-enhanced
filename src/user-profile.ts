import { ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { createElement, waitForElement } from "../typescript-lib/src/shared/dom"
import { UserProfileMeExpander } from "./components/user-profile-me-expander"
import { settings } from "./constants/constants"
import { gameModeCodeNames, UserInfo } from "./constants/types"
import { useFeature } from "./settings"

/**
 * This functions intitialises all the userpage related modifications.
 */
export async function insertUserpageModifications() {
    await waitForElement(".page-extra--userpage")

    do {
        useFeature(settings.profile.showExpandMe.storageKey, insertMeSectionExpander)
        useFeature(settings.profile.convertRankToLink.storageKey, convertRankToLink)

        await wait(2000)
    } while (location.pathname.split("/")[1] === "users")
}

async function getUserInfo() {
    // gets user data from data-initial-data
    // any user pages within the array are unsupported
    if (["realtime", "playlists", "modding"].some((e) => window.location.pathname.includes(e))) {
        throw new Error("unsupported path")
    } else {
        return JSON.parse((await waitForElement<HTMLElement>(".js-react--profile-page")).dataset.initialData!) as UserInfo
    }
}

/**
 * inserts the me! section expander
 * @returns
 */
function insertMeSectionExpander() {
    if (document.querySelector(".me-expander") != null) {
        return
    }

    const userpage = ensuredSelector<HTMLElement>(".page-extra--userpage")
    userpage.style.paddingBottom = "35px"
    userpage.append(new UserProfileMeExpander())

    // add eventListener to edit button to reset expander state
    const button = ensuredSelector(".page-extra--userpage .btn-circle--page-toggle")
    if (button.classList.contains("edited")) {
        return
    }
    button.addEventListener("click", () => {
        document.querySelector<UserProfileMeExpander>(".expander")?.toggleState("collapse")
    })
    button.classList.add("edited")
}

/**
 * converts the rank on user profiles to links to the respective (country) ranking page
 * if the rank is over 10000 it is skipped, as only the first 200 pages of 50 results each are shown
 */
async function convertRankToLink() {
    if (["realtime", "playlists", "modding"].some((e) => window.location.pathname.includes(e))) {
        return // modding, playlists and multiplayer pages do not contain ranks
    }
    const rankElements = document.querySelectorAll<HTMLDivElement>(".profile-detail__chart-numbers .value-display--rank .value-display__value div:not(.rank-to-link-checked)")
    rankElements.forEach((e) => e.classList.add("rank-to-link-checked")) // adds a class in case the rank is outside the conversion scope
    if (rankElements.length != 2) {
        // else this runs in an endless loop as it can no longer find the original elements
        return
    }
    const userInfo = await getUserInfo()
    const globalRankElement = rankElements[0]
    const countryRankElement = rankElements[1]

    let gamemode = userInfo.user.playmode
    if (window.location.pathname.match(/\/users\/[0-9]*\/(fruits|osu|taiko|mania)/gm)) {
        gamemode = window.location.pathname.split("/")[3] as gameModeCodeNames
    }

    const globalRank = userInfo.user.statistics.global_rank
    const countryRank = userInfo.user.statistics.country_rank
    if (globalRank != null && globalRank < 200 * 50) {
        const newGlobalRankElement = createElement("a", {
            attributes: {
                href: `https://osu.ppy.sh/rankings/${gamemode}/performance?page=${Math.floor(globalRank / 50) + 1}#scores`,
                title: "",
                innerText: globalRankElement.innerText,
            },
        })

        newGlobalRankElement.setAttribute("data-html-title", globalRankElement.dataset.htmlTitle!)

        globalRankElement.replaceWith(newGlobalRankElement)
    }
    if (countryRank != null && countryRank < 200 * 50) {
        const newCountryRankElement = createElement("a", {
            attributes: {
                href: `https://osu.ppy.sh/rankings/${gamemode}/performance?page=${Math.floor(countryRank / 50) + 1}&country=${userInfo.user.country_code}#scores`,
                title: "",
                innerText: countryRankElement.innerText,
            },
        })

        newCountryRankElement.setAttribute("data-html-title", countryRankElement.dataset.htmlTitle!)

        countryRankElement.replaceWith(newCountryRankElement)
    }
}
