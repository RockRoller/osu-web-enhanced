import { createElement, createIcon } from "../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../typescript-lib/src/shared/themes"

export class OsuInputLabel extends HTMLLabelElement {
    /**
     *
     * @param id ID to connect input and label
     * @param input Input connected to this label.
     * @param label Label of the label
     */
    constructor(id: string, input: HTMLElement, label: string, extraInfo?: string) {
        super()

        input.id = id
        this.htmlFor = id
        this.innerText = label

        this.style.display = "flex"
        this.style.alignItems = "center"
        this.style.justifyContent = "space-between"
        this.style.minWidth = "72px"
        this.style.padding = "0px"
        this.style.color = osuTheme.normalTextColor
        this.style.fontSize = "14px"
        this.style.textTransform = "unset"
        this.style.lineHeight = "unset"
        this.style.fontWeight = "400"
        this.style.marginBottom = "0"
        this.style.gap = "8px"

        if (extraInfo) {
            this.append(
                createElement("span", {
                    children: [createIcon("info")],
                    attributes: {
                        title: extraInfo,
                    },
                    style: {
                        color: "rgba(255, 255, 255, 0.5)",
                        marginRight: "auto",
                        display: "flex",
                    },
                })
            )
        }

        this.append(input)
    }
}
