import { createElement, createIcon } from "../../typescript-lib/src/shared/dom"
import { editorActionType, Parameter } from "../constants/types"
import { OsuInputLabel } from "./input-label"
import { OsuInputText } from "./input-text"
import { ModalNotificationArea } from "./modal-notification-area"

/**
 * This object defines an element of the message editor for a parameter input pair.
 */
export class CustomMessageParameterInput extends HTMLDivElement {
    /**
     * Input for the name
     */
    private nameInput: OsuInputText

    /**
     * Input for the variable
     */
    private variableInput: OsuInputText

    /**
     * The parameter this objects reflects
     */
    private parameter: Parameter

    /**
     * constructor
     * @param notificationArea The notification area to display notifications in
     * @param editorActionType The type of editor action @see editorActionType
     * @param parameter The parameter, defaults to empty parameter
     */
    constructor(notificationArea: ModalNotificationArea, editorActionType: editorActionType, parameter: Parameter = { label: "", parameter: "" }) {
        super()

        this.parameter = parameter

        this.style.display = "flex"
        this.style.alignItems = "center"
        this.className = "parameter-input"

        this.nameInput = new OsuInputText("please enter variable name")
        this.nameInput.value = parameter.label
        this.nameInput.addEventListener("input", () => {
            notificationArea.updateMessage(editorActionType == "new" ? "danger" : "normal", editorActionType == "new" ? "Unsaved Changes" : "Everything allright")
            this.parameter.label = this.nameInput.value
        })

        this.variableInput = new OsuInputText("please enter variable identifier")
        this.variableInput.value = parameter.parameter
        this.variableInput.addEventListener("input", () => {
            notificationArea.updateMessage(editorActionType == "new" ? "danger" : "normal", editorActionType == "new" ? "Unsaved Changes" : "Everything allright")
            this.parameter.parameter = this.variableInput.value
        })

        const deleteButton = createElement("div", {
            attributes: {
                title: "Delete Variable",
                onclick: () => {
                    notificationArea.updateMessage(editorActionType == "new" ? "danger" : "normal", editorActionType == "new" ? "Unsaved Changes" : "Everything allright")
                    this.remove()
                },
            },
            style: {
                padding: "4px",
                cursor: "pointer",
            },
            children: [
                createIcon("trash", {
                    size: 20,
                }),
            ],
        })

        this.append(new OsuInputLabel("name", this.nameInput, "Name"), new OsuInputLabel("identifier", this.variableInput, "Variable"), deleteButton)
    }

    /**
     * This function checks if all inputs in this parameter input have been filled out
     * @returns if all are filled => true, else => false
     */
    public checkInputState() {
        return this.nameInput.value == "" || this.variableInput.value == "" ? false : true
    }

    /**
     * This function returns the parameter this object reflects
     * @returns
     */
    public getParameter() {
        return this.parameter
    }
}
