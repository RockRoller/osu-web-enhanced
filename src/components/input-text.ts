import { osuTheme } from "../../typescript-lib/src/shared/themes"

/**
 * Text input styled for the osu website.
 */
export class OsuInputText extends HTMLInputElement {
    /**
     * constructor
     * @param placeholder optional, placeholder text
     */
    constructor(placeholder = "") {
        super()

        this.type = "text"
        this.placeholder = placeholder

        this.style.padding = "4px"
        this.style.border = "none"
        this.style.outline = "none"
        this.style.borderRadius = "4px"
        this.style.minWidth = osuTheme.inputMinWidth
        this.style.backgroundColor = osuTheme.secondaryBackground
        this.style.color = osuTheme.normalTextColor
        this.style.fontSize = "14px"
        this.style.lineHeight = "19px"
    }
}
