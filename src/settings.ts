import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalHeader } from "../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { createElement, insertStyleTag, waitForElement } from "../typescript-lib/src/shared/dom"
import { UndefinedStorageValueException } from "../typescript-lib/src/shared/exceptions"
import { gmGetValue, gmSetValue } from "../typescript-lib/src/shared/greasemonkey"
import { osuTheme } from "../typescript-lib/src/shared/themes"
import { OsuInputLabel } from "./components/input-label"
import { settings } from "./constants/constants"
import { createToggleInput } from "./shared/dom-styled"

export async function insertSettingsModalOpenButton() {
    await waitForElement(".nav2__col--avatar .simple-menu")

    do {
        await wait(200)
        const settingsButton = document.querySelector<HTMLElement>(".rr-lib.settings-button-enhanced")
        if (settingsButton == null) {
            ensuredSelector(".nav2__col--avatar .simple-menu").append(createSettingsModalOpenButton())
        } else {
            settingsButton.onclick = onSettingsClick
        }
        await wait(500)
    } while (document.querySelector(".rr-lib.settings-button") == null)
}

/**
 * This function initialises all settings by looping over all of them and writing them to storage.
 */
export function initSettings() {
    // splits the object into multiple arrays, one for each category and loops over them
    Object.entries(settings).map((category) => {
        // [0] is the key, [1] is the value that I want
        const options = category[1]
        // splits the category into the separate settings and loops over them
        Object.entries(options).map((s) => {
            // [0] is the key, [1] is the setting object that I want
            const object = s[1]
            // writes the setting if it doesnt exist, in which case it will throw the error
            try {
                gmGetValue(object.storageKey)
            } catch (e) {
                if (e instanceof UndefinedStorageValueException) {
                    gmSetValue(object.storageKey, object.default)
                } else {
                    throw e
                }
            }
        })
    })
    gmSetValue(
        `customMessage-fallback`,
        JSON.stringify({
            messageOptions: {
                message: "Hello world!",
                title: "Hello world!",
            },
            storageKey: `customMessage-fallback`,
        })
    )
}

/**
 * This function resets the saved value for all settings to their default value.
 */
function resetSettings() {
    Object.entries(settings).map((category) => {
        const options = category[1]
        Object.entries(options).map((s) => {
            const object = s[1]
            // overwrites the setting wth the default value
            gmSetValue(object.storageKey, object.default)
        })
    })
}

/**
 * This function resets all toggles to their written states.
 */
function updateToggles() {
    const toggles = document.querySelectorAll(".rr-lib.toggle-input")
    const settingStates: (string | number | boolean)[] = []

    Object.entries(settings).map((category) => {
        const options = category[1]
        Object.entries(options).map((s) => {
            const object = s[1]
            // overwrites the setting wth the default value
            settingStates.push(gmGetValue(object.storageKey))
        })
    })

    Array.from(toggles).map((t, i) => {
        if (settingStates[i] == true) {
            t.classList.add("toggle-active")
        } else if (settingStates[i] == false) {
            t.classList.remove("toggle-active")
        }
    })
}

/**
 * This function inserts the settings modal.
 */
export function insertSettingsModal() {
    insertStyleTag(`
    .toggle-active .toggle { right: 3px !important }
    .toggle-active .toggle-bg { border-color: ${osuTheme.successColor} !important }
    .rr-lib.settings-tab.active { color: ${osuTheme.normalTextColor}; font-weight: bold }
    .rr-lib.settings-tab.active:after { content: ""; display: block; border-radius: 5px; height: 5px; background-color: ${osuTheme.osuPink}; position: absolute; width: 100%; left: 0px; bottom: -3px }
    .rr-modal .settings-modal__tabbed-pane-header.selected { color: ${osuTheme.osuPink} !important }
    .rr-modal .settings-modal__tabbed-pane-header:hover:not(.selected) { color: ${osuTheme.normalTextColor} !important }
    `)

    const modal = new OsuModal(new OsuModalHeader("Script Settings"))
    const modalWrapper = new ModalWrapper(modal)

    modal.addIconButton("refresh", "Reset Settings", () => {
        if (window.confirm("Do you want to reset all settings?")) {
            resetSettings()
            updateToggles()
        }
    })

    const paneSelectors = createElement("div", {
        className: "settings-modal__tabbed-pane-headers-container",
        style: {
            display: "flex",
            gap: "inherit",
            borderBottom: `1px solid ${osuTheme.osuPink}`,
        },
    })

    const pane = createElement("div", {
        className: "settings-modal__pane",
        style: {
            display: "flex",
            flexDirection: "column",
            gap: "inherit",
            width: "500px",
            height: "500px",
            padding: "0 8px",
        },
    })

    Object.entries(settings).forEach((category, ii) => {
        // category[0] is the title of the category, converting that to uppercase for the header
        const sectionName = category[0]

        const selector = createElement("span", {
            className: `settings-modal__tabbed-pane-header`,
            attributes: {
                innerText: sectionName.charAt(0).toUpperCase() + sectionName.slice(1),
                onclick: () => {
                    paneSelectors.childNodes.forEach((node) => (node as HTMLElement).classList.remove("selected"))
                    pane.innerHTML = ""
                    selector.classList.add("selected")

                    // category[1] is an object containing all the settings for that category
                    Object.entries(category[1]).forEach((s) => {
                        const setting = s[1]

                        const toggle = createToggleInput(() => {
                            gmSetValue(setting.storageKey, !gmGetValue(setting.storageKey))
                            toggle.classList.toggle("toggle-active")
                        })
                        if (gmGetValue(setting.storageKey) == true) {
                            toggle.classList.toggle("toggle-active")
                        }

                        pane.append(new OsuInputLabel(setting.storageKey, toggle, setting.label, setting.extraInfo))
                    })
                },
            },
            style: {
                cursor: "pointer",
                padding: "4px",
                color: "rgba(255, 255, 255, 0.5)",
                transition: "ease all 200ms",
                userSelect: "none",
            },
        })

        if (ii == 0) {
            selector.click()
        }
        paneSelectors.append(selector)
    })

    modal.addContent(paneSelectors)
    modal.addContent(pane)

    ensuredSelector("body").append(modalWrapper)
}

function createSettingsModalOpenButton() {
    return createElement("a", {
        className: "simple-menu__item settings-button-enhanced",
        attributes: {
            innerText: "osu-web enhanced",
            onclick: () => onSettingsClick(),
        },
    })
}

function onSettingsClick() {
    document.querySelector(".nav2__col--avatar .simple-menu")?.classList.add("hidden")
    insertSettingsModal()
}

export function useFeature(storageKey: string, feature: Function) {
    if (gmGetValue(storageKey) == true) {
        feature()
    }
}
