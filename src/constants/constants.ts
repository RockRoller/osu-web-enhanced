import { Setting } from "./types"

/**
 * This object defines all the aditional bbcode options and what icon should be used for them.
 * The key is the BBCode tag, the value the icon
 */
export const additionalBBCode = {
    u: "fas fa-underline",
    color: "fas fa-paint-brush",
    spoiler: "fas fa-stream",
    quote: "fas fa-quote-right",
    centre: "fas fa-align-center",
    youtube: "fab fa-youtube",
    audio: "fas fa-music",
    notice: "fas fa-comment-alt",
    code: "fas fa-code",
    profile: "fas fa-user",
}

/**
 * This object defines all the settings for the script.
 * Settings are grouped into categories according to the area they are used in.
 * If a section of the osu! page has at least 2 settings related to it they should be grouped into one section.
 */
export const settings: Record<string, Record<string, Setting>> = {
    profile: {
        showExpandMe: {
            storageKey: `settings-showExpandMe`,
            label: `Show button to expand me! pages`,
            default: true,
        },
        convertRankToLink: {
            storageKey: `settings-convertRankToLink`,
            label: `Convert global / country rank numbers above the ranking graph to links to rankings page`,
            default: true,
            extraInfo: `This only works for the top 10.000, as ranking pages only show up to #10.000.`,
        },
    },
    forum: {
        showSendMessage: {
            storageKey: `settings-showSendMessage`,
            label: `Display 'Send Message' button on every forum post`,
            default: true,
        },
        showAdditionalBBCode: {
            storageKey: `settings-showAdditionalBBCode`,
            label: `Show additional BBCode buttons when creating a new topic/reply`,
            default: false,
        },
        highlightOwnName: {
            storageKey: `settings-highlightOwnName`,
            label: `Highlight own name on forum listing and threads`,
            default: false,
        },
        insertQuoteAtCursorPosition: {
            storageKey: `settings-insertQuoteAtCursorPosition`,
            label: `Insert quotes at current cursor position in topic reply box`,
            default: true,
        },
        showMessageManager: {
            storageKey: `settings-showForumManager`,
            label: `Show forum template manager`,
            default: true,
            extraInfo: `The forum template manager allows you to create, manage and insert templates for forum posts.`,
        },
        quoteSelectedText: {
            storageKey: `settings-quoteSelectedText`,
            label: `Quote by selecting text`,
            default: true,
        },
    },
    beatmaps: {
        showDownloadOnDiscussion: {
            storageKey: `settings-showDownloadOnDiscussion`,
            label: `Show download buttons on discussion page`,
            default: true,
        },
        showDotOsuData: {
            storageKey: `settings-showDotOsuData`,
            label: `Show 'download .osu data' button`,
            default: true,
        },
        showExpandDescription: {
            storageKey: `settings-showExpandDescription`,
            label: `Show 'view full description' button`,
            default: true,
        },
        showDetailedInfo: {
            storageKey: `settings-showDetailedInfo`,
            label: `Show 'view detailed info' button`,
            default: true,
        },
        showBeatmapCoverButton: {
            storageKey: `settings-showBeatmapCoverButton`,
            label: `Show 'open background' button`,
            default: true,
        },
        showOMDBBUtton: {
            storageKey: `settings-showOMDBBUtton`,
            label: `Show 'rate on OMDB' button`,
            default: true,
        },
    },
    other: {
        showMessageManager: {
            storageKey: `settings-showChatManager`,
            label: `Show chat manager.`,
            default: true,
            extraInfo: `The chat manager allows you to create, manage and send templates for chat messages`,
        },
        logAPIdata: {
            storageKey: `settings-logAPIdata`,
            label: `Log API data to console on all pages`,
            default: false,
            extraInfo: `Many pages on the osu! website use hidden <script> tags whose text content contains API responses for the current page, which will get logged to the browser console if this setting is enabled.`,
        },
        openNonPPYLinksExternal: {
            storageKey: `settings-openNonPPYLinksExternal`,
            label: `Open all links that aren't on ppy.sh in a new tab`,
            default: true,
        },
        hideNotificationCount: {
            storageKey: `settings-hideNotifications`,
            label: `Removes the notification count from the tab title`,
            default: false,
        },
    },
}

/**
 * map of all the keys for customMessages
 * would just use the type-template, but that wasn't used for chat so i chose this approach rather than having to reformat all messages
 */
export const templateKeys = {
    forum: "forum-template",
    chat: "message",
}
